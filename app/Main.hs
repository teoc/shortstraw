{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE OverloadedStrings #-}
module Main where
import Control.Monad
import Data.Text.Lazy (Text)
import Text.Megaparsec
import Text.Megaparsec.Char
import Text.Megaparsec.Char.Lexer hiding (space)
import Data.Void
import Data.List ( init, foldl', maximumBy )
import Data.Map (Map)
import Data.Maybe
import Data.Function
import Data.Functor (($>))
import Data.Foldable (traverse_)
import Control.Arrow (first, second)
import Data.Text (pack)
import Data.ByteString (writeFile)

import WeighedNode
import VerboseLogParser

import Simulate (simulate, ModEvent (..))
import ChromeTracingExport

import qualified Data.Text.Lazy.IO as LazyIO
import qualified Data.Map as M
import qualified Jsonifier as JSON
import Prelude hiding (writeFile)

main :: IO ()
main = do
  stdin <- LazyIO.getContents
  case parse parser "stdin" stdin of
    Left err -> print $ errorBundlePretty err
    Right lines ->
      let
        mg = head $ mapMaybe toMG lines
        eops = mapMaybe toEOP lines
        wgraph = mkWeighedGraph mg eops
        timePath = critPath wgraph nodeEopTime
      in do
        -- print lines
        -- print wgraph
        print timePath
        traverse_ (print . first (/1000)) $ snd timePath
        print $ fst timePath / 1000
        writeFile "out.json" $ JSON.toByteString $ JSON.array $ map (\ModEvent{..} -> mkCompleteEvent (pack eventModName) "module" (eventStart * 1000) ((eventEnd - eventStart) * 1000) 1 eventThreadId) $ simulate wgraph


mkWeighedGraph :: ModGraph -> [EndOfPhase]-> WeighedGraph
mkWeighedGraph ModGraph {..} =
  foldl' (flip addWeight) (M.fromList $ map convert modSums)
  where
    convert ModSum {..} =
      (msModName, WeighedNode
        { nodeDeps = map snd msTextImports
        , nodeEopTime = 0
        , nodeAllocs = 0
        })
    addWeight EndOfPhase{..} =
      M.adjust (\wn -> wn {nodeEopTime = eopTime + nodeEopTime wn, nodeAllocs = eopAllocs + nodeAllocs wn}) eopModuleName

critPath :: WeighedGraph -> (WeighedNode -> Double) -> (Double, [(Double, ModName)])
critPath wg measure = maxFst . M.elems $ critPathGraph
  where
    maxFst [] = (0, [])
    maxFst xs = maximumBy (compare `on` fst) xs
    critPathGraph = M.mapWithKey critPathFrom wg
    lookupCrit key = M.lookup key critPathGraph
    critPathFrom nodeName node = (measure node + restMeasure, (measure node, nodeName):restPath)
      where
       (restMeasure, restPath) = maxFst . mapMaybe lookupCrit $ nodeDeps node
