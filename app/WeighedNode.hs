module WeighedNode where
import Data.Map.Strict (Map)

type ModName = String

data WeighedNode =
  WeighedNode
    { nodeDeps :: [ModName]
    , nodeEopTime :: !Double
    , nodeAllocs :: !Double
    }
  deriving (Show)

type WeighedGraph = Map ModName WeighedNode
