# Shortstraw -- a tool for analyzing GHC parallel builds
## Overview

There are broadly two ways in which GHC can build haskell projects in parallel.
  1) Package/unit level parallelism
     
     This is enabled by passing "-j<N>" to cabal-install/stack.
  
  2) Module level parallelism
  
     This is enabled by passing "-j<N>" to GHC, eg, via "--ghc-options".

This package deals with the second type of parallelism. 
This type of parallelism is crucial for compilling large Haskell packages in a reasonable amount of time.

Yet, the effectiveness of this parallelism is limited by the critical path of the package.
A linear chain of heavy modules can easily starve this parallelism.

This package gives us tools to analyze this critical path.


## Status
This project is quite unstable. At the moment it only caters to my specific needs.
If you are interested in a different type of analysis, please open an issue and I'd be happy to discuss it.

## How to use

The executable exposed by this package reads verbose logs from GHC on stdin and outputs the critical path on stdout.

To create these logs you need to pass `-v2` to GHC. I also recommend running GHC with `-j1` as this gives the most accurate statistics and avoids issues with lines being mangled by parallel logging.
