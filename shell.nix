{ pkgs ? import <nixpkgs> {} }:

with pkgs;

mkShell {
  buildInputs = [
    pkgs.ghc
    pkgs.cabal-install
    pkgs.entr
  ];
}
